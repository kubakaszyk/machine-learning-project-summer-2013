function [y, fraction_output_correct] = multinet(x_train, y_train, x_test, y_test, num_hidden_layer_nodes, convergence_criterion, eta, max_iterations)
    % Binary Multi-layer neural network that distinguishes between 2's and 3's. 
    % Trained using backpropagation
    %
    %
    % Input: 
    % N cases and D features
    % x_train, y_train - training set
    % x_test, y_test - testing set
    % convergence_criterion - accuracy required to finish training
    % eta - learning rate
    % iterations - maximum number of training iterations
    % good eta ~ ?
    %
    % Ouput: 
    % w_ih - weights from input to hidden layer
    % w_ho - weights from hidden layer to output
    % y - output of neural network
    % c - classes output(2 or 3)

    tic
    

    % Convert to 0,1
    [x_train, y_train, x_test, y_test] = convert_to_bin_01(x_train, y_train, x_test, y_test);

    % Backpropagation
    [w_ih, w_ho] = backprop(x_train, y_train, num_hidden_layer_nodes, convergence_criterion, eta, max_iterations);

    % Output
    h = x_test*w_ih;
    o = h*w_ho;
    hidden = bsxfun(@rdivide, exp(-h),sum(exp(-h), 2));
    output = bsxfun(@rdivide, exp(-o),sum(exp(-o), 2));

    % Fraction Correct
    c = (output > 0.5);

    [o output]

    fraction_output_correct = mean(c == y_test)


    toc
