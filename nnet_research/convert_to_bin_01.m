function [x_train2, y_train2, x_test2, y_test2] = convert_to_bin_01(x_train, y_train, x_test, y_test)
    % Add bias parameter - arbitrarily chosen 0.5
    [x_train_row, x_train_col] = size(x_train);
    train_bias = ones(x_train_row,1)*0.5;
    x_train = [train_bias, x_train];

    [x_test_row, x_test_col] = size(x_test);
    test_bias = ones(x_test_row,1)*0.5;
    x_test = [test_bias, x_test];


    % Save sizes of data
    [N, E] = size(x_test);
    [M, Q] = size(x_train);
    [A, B] = size(y_train);
    [C, D] = size(y_test);


    % Convert classes to 0,1, where 1 is the class and 0 is not the class
    y_train2 = [];
    x_train2 = [];
    for x=1:A
        if y_train(x) == 2
            y_train2 = [y_train2; [1,0]];
            x_train2 = [x_train2; x_train(x, :)];
        elseif y_train(x) == 3
            y_train2 = [y_train2; [0,1]];
            x_train2 = [x_train2; x_train(x, :)];
        end
    end

    y_test2 = [];
    x_test2 = [];
    for x=1:C
        if y_test(x) == 2
            y_test2 = [y_test2; [1,0]];
            x_test2 = [x_test2; x_test(x, :)];
        elseif y_test(x) == 3
            y_test2 = [y_test2; [0,1]];
            x_test2 = [x_test2; x_test(x, :)];
        end
    end

