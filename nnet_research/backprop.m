function [w_ih, w_ho] = backprop(x_train, y_train, num_hidden_layer_nodes, convergence_criterion, eta, max_iterations)    

    % Get Sizes 
    [N_x, D_x] = size(x_train);
    [N_y, D_y] = size(y_train);

    % Set initial weights
    w_ih = rand(D_x, num_hidden_layer_nodes);
    w_ho = rand(num_hidden_layer_nodes, D_y);

    w_ih = 0.01*(w_ih - mean(mean(w_ih)));
    w_ho = 0.01*(w_ho - mean(mean(w_ho)));


    % Backprop
    err = convergence_criterion+1;
    count = 0;

    hidden = bsxfun(@rdivide, exp(-(x_train*w_ih)),sum(exp(-(x_train*w_ih)), 2)); % Softmax
    output = bsxfun(@rdivide, exp(-(hidden*w_ho)),sum(exp(-(hidden*w_ho)), 2));

    while err > convergence_criterion & count < max_iterations
        for nx=1:N_x
            h = x_train*w_ih;
            o = h*w_ho;
            hidden = bsxfun(@rdivide, exp(-h),sum(exp(-h), 2));
            output = bsxfun(@rdivide, exp(-o),sum(exp(-o), 2));

            output_error = output(nx,:).*(1-output(nx,:)).*(y_train(nx,:) - output(nx,:));
            hidden_error = hidden(nx,:).*(1-hidden(nx,:)).*(w_ho*output_error')';

            delta_ih = eta*x_train(nx,:)'*hidden_error;
            delta_ho = eta*hidden(nx,:)'*output_error;

            w_ih = w_ih - delta_ih;
            w_ho = w_ho - delta_ho;


        end        

        err = 1/2*sum(sum((output - y_train).^2));
        count = count+1;
    end

end
