function [x_train2, y_train2, x_test2, y_test2] = convert_to_bin(x_train, y_train, x_test, y_test, classify)
    
    % Add bias parameter - arbitrarily chosen 0.5
    [x_train_row, x_train_col] = size(x_train);
    train_bias = ones(x_train_row,1)*0.5;
    x_train = [train_bias, x_train];

    [x_test_row, x_test_col] = size(x_test);
    test_bias = ones(x_test_row,1)*0.5;
    x_test = [test_bias, x_test];

    y_train2 = [];
    x_train2 = [];
    y_test2 = [];
    x_test2 = [];
    ins = zeros(1,10);
    
    for k=1:x_train_row
        if ismember(y_train(k), classify)
            y_train2 = [y_train2;ins];
            y_train2(k, y_train(k)+1) = 1;
            x_train2(k,:) = x_train(k,:);
        end
    end

    for k=1:x_test_row
        if ismember(y_test(k), classify)
            y_test2 = [y_test2;ins];
            y_test2(k, y_test(k)+1) = 1;
            x_test2(k,:) = x_test(k,:);
        end
    end
end
