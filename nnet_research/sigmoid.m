function [w, y, c, num] = sigmoid(x_train, y_train, x_test, y_test, c1, eta, it)
    % Sigmoid binary single layer neural network that distinguishes between 2's and 3's. 
    % Trained using gradient descent. 
    % 
    %
    % Input: 
    % x_train, y_train - training set
    % x_test, y_test - testing set
    % c1 - convergence criterion
    % eta - learning rate
    % it - maximum number of training iterations
    % Small eta ~ 0.000009
    %
    % Ouput: 
    % w - weights
    % y - output of neural network
    % c - classes output(2 or 3)
    % num - percentage correct
    %
    % 
    % Trained using greyscale 256x256 images of digits, achieves up to 97% accuracy. 
    %
    %

    tic
    % Add bias parameter - arbitrarily chosen 0.5
    [x_train_row, x_train_col] = size(x_train);
    bias_train = ones(x_train_row,1)*0.05;
    x_train = [bias_train, x_train];

    [x_test_row, x_test_col] = size(x_test);
    bias_test = ones(x_test_row,1)*0.05;
    x_test = [bias_test, x_test];

    [N, E] = size(x_test);
    [M, Q] = size(x_train);
    [A, B] = size(y_train);
    [C, D] = size(y_test);

    % Convert classes to 0-1 - Single output
    y_train2 = [];
    x_train2 = [];
    for x=1:A
        if y_train(x) == 2
            y_train2 = [y_train2; 1];
            x_train2 = [x_train2; x_train(x, :)];
        elseif y_train(x) == 3
            y_train2 = [y_train2; 0];
            x_train2 = [x_train2; x_train(x, :)];
        end
    end

    y_test2 = [];
    x_test2 = [];
    for x=1:C
        if y_test(x) == 2
            y_test2 = [y_test2; 1];
            x_test2 = [x_test2; x_test(x, :)];
        elseif y_test(x) == 3
            y_test2 = [y_test2; 0];
            x_test2 = [x_test2; x_test(x, :)];
        end
    end


    [N, E] = size(x_test2);
    [M, Q] = size(x_train2);
    [A, B] = size(y_train2);
    [C, D] = size(y_test2);

    % Gradient Descent
    w = rand(B, Q)/100;

    %size(ones(M,1)*0.05);

    y = (1.0./(1+exp(-(w*x_train2' + (ones(M,1)*0.05)')')));
    err = 1/2*sum((y - y_train2).^2);
 
    count = 0;
    while(err > c1 & count <= it)
       count = count + 1;
       wdelta = zeros(B,Q); % Initialize wdelta to zeros
       y = (1.0./(1+exp(-(w*x_train2' + (ones(M,1)*0.05)')'))); % Compute the predicted output based on initial weights
       delta = y - y_train2; % Compute difference between predicted and actual 
       wdelta = sum(bsxfun(@times, y, 1-y))*delta'*x_train2; % Compute the change in weights between iterations
       w = w - eta*wdelta; % Adjust the weight by subtracting the product of the learning rate and the change in weights
       
       err = 1/2*sum((y - y_train2).^2);
    
    end

    % Classify
    y = (1.0./(1+exp(-(w*x_test2' + 0.05)')'))'
    s = size(y)


    % Interpret Neural Network output
    num = 0;
    c = [];
    for a=1:N
        if (y(a)>0.5)
            c(a) = 1;
        else
            c(a) = 0; 
        end
        if c(a) == y_test2(a)
            num = num+1;
        end
    end
    

    num = num/N;
    toc

end
