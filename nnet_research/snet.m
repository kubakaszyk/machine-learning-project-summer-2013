function [w, y, c, num] = snet(x_train, y_train, x_test, y_test, convergence_criterion, eta,iterations)
    % Linear binary single layer neural network that distinguishes between 2's and 3's. 
    % Trained using gradient descent. 
    %
    %
    % Input: 
    % x_train, y_train - training set
    % x_test, y_test - testing set
    % convergence_criterion - accuracy required to finish training
    % eta - learning rate
    % iterations - maximum number of training iterations
    % Small eta ~ 0.000009
    %
    % Ouput: 
    % w - weights
    % y - output of neural network
    % c - classes output(2 or 3)
    % num - percentage correct
    %
    % 
    % Trained using greyscale 256x256 images of digits, achieves up to 97% accuracy. 
    % 
    %

    tic
    % Add bias parameter - arbitrarily chosen 0.5
    [x_train_row, x_train_col] = size(x_train);
    train_bias = ones(x_train_row,1)*0.5;
    x_train = [train_bias, x_train];

    [x_test_row, x_test_col] = size(x_test);
    test_bias = ones(x_test_row,1)*0.5;
    x_test = [test_bias, x_test];


    % Save sizes of data
    [N, E] = size(x_test);
    [M, Q] = size(x_train);
    [A, B] = size(y_train);
    [C, D] = size(y_test);


    % Convert classes to 0,1, where 1 is the class and 0 is not the class
    y_train2 = [];
    x_train2 = [];
    for x=1:A
        if y_train(x) == 2
            y_train2 = [y_train2; [1,0]];
            x_train2 = [x_train2; x_train(x, :)];
        elseif y_train(x) == 3
            y_train2 = [y_train2; [0,1]];
            x_train2 = [x_train2; x_train(x, :)];
        end
    end

    y_test2 = [];
    x_test2 = [];
    for x=1:C
        if y_test(x) == 2
            y_test2 = [y_test2; [1,0]];
            x_test2 = [x_test2; x_test(x, :)];
        elseif y_test(x) == 3
            y_test2 = [y_test2; [0,1]];
            x_test2 = [x_test2; x_test(x, :)];
        end
    end

    % Save sizes
    [N, E] = size(x_test2);
    [M, Q] = size(x_train2);
    [A, B] = size(y_train2);
    [C, D] = size(y_test2);



    % Gradient Descent

    % Weight matrix
    w = rand(B, Q); % BxQ == number_of_possible_outputs(classes to choose from) x number_of_features_in_feature_vector

    err = 1/2*sum(((w*x_train2')' - y_train2).^2); % Error function - square difference between the prediction and the actual class. At this point weights are random.

 
    count = 0; % Tracks the number of times the loop iterates
    while(err > convergence_criterion & count <= iterations)
       count = count + 1;
       wdelta = zeros(B,Q); % Initialize wdelta to zeros, BxQ == number_of_possible_outputs(classes to choose from) x number_of_features_in_feature_vector
                            % wdelta is change in delta, calculated per iteration
       y = (w*x_train2')'; % Compute the predicted output based on initial weights
       delta = y-y_train2; % Compute difference between predicted and actual
       wdelta = delta'*x_train2; % Compute the change in weights between iterations
       w = w - eta*wdelta; % Adjust the weight by subtracting the product of the learning rate and the change in weights
       
       err = 1/2*sum(((w*x_train2')' - y_train2).^2); % Compute error
    end

    y = (w*x_test2')'; % Compute output of test set
    

    % Check test prediction against actual test output
    num = 0;
    c = [];
    for a=1:N
        if(y(a,1) > y(a,2))
           c = [c;[1 0]];
        else
           c = [c;[0 1]];
        end
        if c(a,:) == y_test2(a,:)
            num = num + 1;
        end
    end
    num = num/N;

    toc
    

end 
